const container = document.getElementById("2nd-container");
const backBtn = document.getElementById("back-btn");
const darkModeBtn = document.getElementById("dark-mode");
const loader = document.getElementById("loader");

function getQueryParameters(param) {
  let urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(param);
}

backBtn.addEventListener("click", () => {
  window.history.back();
});

darkModeBtn.addEventListener("click", toggleDarkMode);
function toggleDarkMode() {
  document.body.classList.toggle("dark-mode");
}

let countryname = getQueryParameters("countryname");

let countriesData = [];

showLoader();
function fetchData(url) {
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      countriesData = data;
      let country = data.filter((country) => {
        let countryName = country.name.common;
        return countryName.toLowerCase() === countryname.toLowerCase();
      });

      countryInfo(country);
      showContent();
    })
    .catch((error) => console.error("Error:", error));
}

fetchData("https://restcountries.com/v3.1/all");
// ----------------functions--------------
function countryInfo(data) {
  data = data[0];
  let imgDiv = document.createElement('div');
  imgDiv.className = 'w-full lg:w-1/2 p-4';

  let img = document.createElement("img");
  img.src = data["flags"]["png"];
  img.alt = data.name.common;
  img.className = "w-full h-auto rounded-lg";

  let div = document.createElement("div");
  div.className = "flex flex-col justify-start p-4 lg:w-1/2";

  let h1 = document.createElement("h1");
  h1.className = "text-2xl font-bold my-4";
  h1.textContent = data.name.common;

  let select = document.createElement("section");
  select.className = "flex flex-col justify-start flex-wrap";
  select.innerHTML = `
    <p class='p-2'><b>Capital:</b> ${data.capital}</p>
    <p class='p-2'><b>Region:</b> ${data.region}</p>
    <p class='p-2'><b>Subregion:</b> ${data.subregion}</p>
    <p class='p-2'><b>Population:</b> ${data.population}</p>
    <p class='p-2'><b>Top Level Domain:</b> ${data.tld[0]}</p>
    <p class='p-2'><b>Currencies:</b> ${Object.values(data.currencies)[0].name}</p>
    <p class='p-2'><b>Languages:</b> ${Object.values(data.languages).join(", ")}</p>
  `;

  let h3 = document.createElement("h3");
  h3.className = "text-lg font-bold my-4";
  h3.textContent = "Border Countries:";

  let borders = data.borders ? data.borders : [];
  let borderList = document.createElement("ul");
  borderList.className = "flex flex-wrap justify-start font-normal";
  borders.forEach((border) => {
    let button = document.createElement("button");
    button.textContent = border;
    button.className = "p-2 m-2 border border-gray-300 rounded-md";
    button.addEventListener("click", countryBorder);
    borderList.appendChild(button);
  });
  h3.appendChild(borderList);

  imgDiv.appendChild(img);
  container.appendChild(imgDiv);
  div.appendChild(h1);
  div.appendChild(select);
  div.appendChild(h3);
  container.appendChild(div);
}

function countryBorder(event) {
  let selectedCountry = event.target.textContent.toLowerCase();
  countriesData.forEach((country) => {
    if (country["cca3"].toLowerCase() === selectedCountry) {
      window.location.href = `country-info.html?countryname=${country["name"]["common"]}`;
    }
  });
}

function showLoader() {
  loader.style.display = "block";
  container.style.display = "none";
}

function showContent() {
  loader.style.display = "none";
  container.style.display = "flex";
}
