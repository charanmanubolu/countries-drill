const countries = document.getElementById("countries");
const searchBar = document.getElementById("search");
const darkModeBtn = document.getElementById("dark-mode");
const cardContainer = document.querySelectorAll(".card-container");
const container = document.getElementById("container");
const loader = document.getElementById("loader");

const selectbtn = document.getElementById("selectbtn");

searchBar.addEventListener("keyup", filterItems);
darkModeBtn.addEventListener("click", toggleDarkMode);

selectbtn.addEventListener("click", country);

showLoader();

function fetchData(url) {
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      countryDetailes(data);
      showContent();
    })
    .catch((error) => console.error("Error:", error));
}
fetchData("https://restcountries.com/v3.1/all");
// ------------functions--------------
function toggleDarkMode() {
  document.body.classList.toggle("dark-mode");
}

function countryDetailes(data) {
  if (Array.isArray(data)) {
    data.forEach((country) => {
      let countryName = `${country.name.common}`;
      let region = `${country.region}`;
      let population = `${country.population}`;
      let capital = `${country.capital}`;
      let flag = `${country.flags.png}`;

      let card = document.createElement("div");
      card.className = "w-full sm:w-1/2 md:w-3/3 lg:w-3/4 bg-white mb-10 pb-1 card-container box-border m-b-5 m-0 p-4 shadow-lg rounded-lg";
      let h2 = document.createElement("h1");
      h2.className = "font-bold p-1 text-xl";
      h2.textContent = `${countryName}`;
      h2.setAttribute("id", "countryName");
      let p1 = document.createElement("p");
      p1.className = "p-1";
      p1.textContent = `Population: ${population}`;
      let p2 = document.createElement("p");
      p2.setAttribute("id", "region");
      p2.className = "p-1";
      p2.textContent = `Region: ${region}`;
      let p3 = document.createElement("p");
      p3.className = "p-1";
      p3.textContent = `Capital: ${capital}`;
      let img = document.createElement("img");
      img.className = "w-full h-48 object-cover rounded-t-lg";
      img.src = `${flag}`;
      img.alt = `${countryName} flag`;

      card.appendChild(img);
      card.appendChild(h2);
      card.appendChild(p1);
      card.appendChild(p2);
      card.appendChild(p3);

      countries.append(card);

      card.addEventListener("click", () => {
        countryDetailsModal(country);
      });
    });
  }
}

function filterItems(event) {
  let text = event.target.value.toLowerCase();
  let countriesName = document.querySelectorAll("#countryName");

  Array.from(countriesName).forEach((item) => {
    let itemName = item.textContent.toLowerCase();
    if (itemName.indexOf(text) === -1) {
      item.parentElement.style.display = "none";
    } else {
      item.parentElement.style.display = "block";
    }
  });
}

function country() {
  let input = document.getElementById("selectbtn").value;
  let countriesRegions = document.querySelectorAll("#region");

  Array.from(countriesRegions).forEach((item) => {
    let itemName = item.textContent;

    if (itemName.indexOf(input) === -1) {
      item.parentElement.style.display = "none";
    } else {
      item.parentElement.style.display = "block";
    }
  });
}

function countryDetailsModal(data) {
  let name = data.name.common;
  window.location.href = `country-info.html?countryname=${name}`;
  console.log(name);
}

function showLoader() {
  loader.style.display = "block";
  container.style.display = "none";
}

function showContent() {
  loader.style.display = "none";
  container.style.display = "block";
}
